<?php

/**
 * @file
 * Contains Drupal\leafwriter\Form\LeafWriterConfigurationForm.
 */

namespace Drupal\leaf_writer\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media\Entity\Media;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;

/**
 * Class EditXMLMediaContentForm.
 *
 * @package Drupal\leaf_writer\Form
 */
class EditXMLMediaContentForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'leafwriter_media_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm($form, FormStateInterface $form_state, $node = NULL) {
    $lock_service = \Drupal::service('content_lock');
    $entity = $node;
    $entity_type = $entity->getEntityTypeId();
    $user = \Drupal::currentUser();
    $node_id = $node->id();
    // Get Media Item of current Node with fits_technical_metadata reference
    $mediaStorage = \Drupal::entityTypeManager()->getStorage('media');
    $mids = $mediaStorage->getQuery()
      ->condition('bundle', 'document')
      ->condition('field_media_of', $node_id)
      ->range(0, 1)
      ->sort('created', 'DESC')
      ->execute();

    // Get media entities.
    if (!empty($mids)) {
      $media_id = reset($mids);
      if ($media_id) {
        // Load media and get actual file path
        $media = Media::load($media_id);
      }
    }
    $form['node_entity'] = [
      '#type' => 'hidden',
      '#value' => $node_id,
    ];
    $form['media_entity'] = [
      '#type' => 'hidden',
      '#value' => $media_id,
    ];
    /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $form_display */
    if (!$lock_service->locking($entity->id(), $entity->language()->getId(), 'edit-content', $user->id(), $entity_type)) {
      $entity_form_display = \Drupal::entityTypeManager()->getStorage('entity_form_display')->load('media.document.repository_xml_disable');
    }
    else {
      $entity_form_display = \Drupal::entityTypeManager()->getStorage('entity_form_display')->load('media.document.repository_xml');
    }
    $form['#parents'] = [];
    if ($widget = $entity_form_display->getRenderer('name')) {
      $items = $media->get('name');
      $items->filterEmptyItems();
      $form['name'] = $widget->form($items, $form, $form_state);
      $form['name']['#access'] = $items->access('edit');
    }

    if ($file_widget = $entity_form_display->getRenderer('field_media_document')) {
      $items = $media->get('field_media_document');
      $items->filterEmptyItems();
      $form['field_media_document'] = $file_widget->form($items, $form, $form_state);
      $form['field_media_document']['#access'] = $items->access('edit');
    }

    // We lock the content if it is currently edited by another user.
    if (!$lock_service->locking($entity->id(), $entity->language()->getId(), 'edit-content', $user->id(), $entity_type)) {
      $form['#disabled'] = TRUE;
      // Do not allow deletion, publishing, or unpublishing if locked.
      foreach (['delete', 'publish', 'unpublish'] as $key) {
        if (isset($form['actions'][$key])) {
          unset($form['actions'][$key]);
        }
      }
      // If moderation state is in use also disable corresponding buttons.
      if (isset($form['moderation_state'])) {
        unset($form['moderation_state']);
      }
    }
    else {
      // ContentLock::locking() returns TRUE if the content is locked by the
      // current user. Add an unlock button only for this user.
      $form['actions']['unlock'] = $lock_service->unlockButton($entity_type, $entity->id(), $entity->language()->getId(), 'edit-content', \Drupal::request()->query->get('destination'));
    }

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_value = $form_state->getValues();
    $media_entity = Media::load($form_value['media_entity']);
    $media_entity->set('name', $form_value['name']);
    $media_entity->save();
    \Drupal::messenger()->addStatus('Your changes has been saved.');
    // Unlock content for editing.
    $node_entity = Node::load($form_value['node_entity']);
    if($node_entity) {
      $lock_service = \Drupal::service('content_lock');
      $lock_service->release($form_value['node_entity'], $node_entity->language()->getId());
    }
    $url = Url::fromRoute('entity.node.canonical', ['node' => $form_value['node_entity']]);
    $form_state->setRedirectUrl($url);

  }
}
