<?php

namespace Drupal\leaf_group\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates that the author of the group of type personal workspace has only
 * one.
 */
class GroupPersonalWorkspaceOwnerUniqueValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The group entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $groupStorage;


  /**
   * GroupPersonalWorkspaceOwnerUniqueValidator constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->groupStorage = $entity_type_manager->getStorage('group');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * @inheritDoc
   */
  public function validate($value, Constraint $constraint) {
    /** @var \Drupal\group\Entity\GroupInterface $entity */
    $entity = $value;
    $entity_type_id = $entity->getEntityTypeId();
    $bundle = $entity->bundle();
    $targeted_bundle = 'personal_workspace';
    if ($entity_type_id !== 'group' || $bundle !== $targeted_bundle) {
      return;
    }

    $query = $this->groupStorage->getQuery();
    $query->accessCheck(FALSE);

    $entity_id = $entity->id();
    if (isset($entity_id)) {
      $id_key = $entity->getEntityType()->getKey('id');
      $query->condition($id_key, $entity_id, '<>');
    }

    $bundle_key = $entity->getEntityType()->getKey('bundle');
    $owner_key = $entity->getEntityType()->getKey('owner');
    $user_has_workspace = $query->condition($bundle_key, $targeted_bundle)
      ->condition($owner_key, $entity->getOwnerId())
      ->range(0, 1)
      ->count()
      ->execute();
    if ($user_has_workspace) {
      $this->context->addViolation($constraint->message, [
        '%name' => $entity->getOwner()->getDisplayName(),
      ]);
    }
  }

}
