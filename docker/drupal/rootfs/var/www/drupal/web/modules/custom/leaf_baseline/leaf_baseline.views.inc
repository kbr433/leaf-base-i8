<?php

/**
 * @file
 * The module file.
 */

/**
 * Implements hook_views_data().
 */
function leaf_baseline_views_data() {
  $data['views']['table']['group'] = t('Default Thumbnail');
  $data['views']['table']['join'] = [
    '#global' => [],
  ];

  $data['views']['default_thumbnail_field'] = [
    'title' => t('Default Thumbnail'),
    'help' => t('The Default Thumbnail for repository item nodes.'),
    'field' => [
      'title' => t('Default Thumbnail'),
      'help' => t('Default thumbnail field.'),
      'id' => 'default_thumbnail_field',
    ],
  ];
  return $data;
}
