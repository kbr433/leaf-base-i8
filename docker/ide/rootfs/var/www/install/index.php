<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="refresh" content="1;">
    <title>Installing</title>
  </head>
  <body>
    <h1>Please wait while the site installs</h1>
    <p>Install started at: <?php echo (shell_exec("s6-svstat -o updownsince /run/s6/services/nginx | s6-tai64nlocal")); ?></p>
    <p>Time elapsed: <?php echo (gmdate("H:i:s", exec("s6-svstat -t /run/s6/services/nginx"))); ?></p>
  </body>
</html>
