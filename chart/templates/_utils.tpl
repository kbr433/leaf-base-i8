
{{/* vim: set filetype=mustache: */}}

{{- /*
Merges a list of YAML documents giving precedence from right to left.
*/ -}}
{{- define "utils.yaml.merge" -}}
{{- $result := dict -}}
{{- range . -}}
{{- $result := mergeOverwrite $result (fromYaml .) -}}
{{- end -}}
{{ toYaml $result }}
{{- end -}}
