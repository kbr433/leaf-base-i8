<?php

namespace Drupal\leaf_baseline\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * A ORCID text field widget.
 *
 * @FieldWidget(
 *   id = "orcid_textfield",
 *   label = @Translation("Orcid Textfield"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class OrcidTextField extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = isset($items[$delta]->value) ? $items[$delta]->value : '';
    $element += [
      '#type' => 'textfield',
      '#default_value' => $value,
      '#size' => 60,
      '#maxlength' => 60,
      '#title' => $element['#title'],
      '#description' => $element['#description'],
      '#field_prefix' => 'https://orcid.org/',
      '#attributes' => array(
        'class' => array('text-full', 'text-orcid-element', 'masked'),
        'placeholder' => 'XXXX-XXXX-XXXX-XXXX',
        'pattern' => '[0-9]{4}[\-][0-9]{4}[\-][0-9]{4}[\-][0-9X]{4}$',
        'data-charset' => 'XXXX-XXXX-XXXX-XXX?',
        'title' => t('The ORCID iD is expressed as an https URI, i.e. the 16-digit identifier is preceded by "https://orcid.org/". A hyphen is inserted every 4 digits of the identifier to aid readability. The last character in the ORCID iD is a checksum. In accordance with ISO/IEC 7064:2003, MOD 11-2, this checksum must be "0" - "9" or "X", a capital letter X which represents the value 10.'),
      ),
      '#element_validate' => [
        [static::class, 'validate'],
      ],
    ];
    $form['#attached']['library'][] = 'leaf_baseline/estelle';
    return ['value' => $element];
  }

  /**
   * Validate the Orcid text field widget input.
   */
  public static function validate($element, FormStateInterface $form_state) {
    if ($element['#value'] !== '') {
      $label = $element['#title'];
      $value = $element['#value'];
      $url = 'https://orcid.org/' . $value;
      $pattern = '/\d{4}[\-]\d{4}[\-]\d{4}[\-][0-9X]{4}$/';
      if (!preg_match($pattern, $value)) {
        $title = $element['#attributes']['title'];
        $form_state->setError($element, t('@label is not a valid ORCID iD format. @title.', [
          '@label' => $label,
          '@title' => $title,
        ]));
      }
      elseif (strpos(@get_headers($url)[0], '200') === FALSE) {
        $form_state->setError($element, t('@label is not a valid ORCID iD. @url could not be found!', [
          '@label' => $label,
          '@url' => $url,
        ]));
      }
    }
  }

}
