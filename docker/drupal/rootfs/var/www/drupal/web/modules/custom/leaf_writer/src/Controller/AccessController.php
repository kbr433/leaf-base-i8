<?php

/**
 * @file
 * Contains Drupal\leafwriter\Form\LeafWriterConfigurationForm.
 */

namespace Drupal\leaf_writer\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\node\NodeInterface;

/**
 * Builds an example page.
 */
class AccessController {

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(NodeInterface $node) {
    // If node is repository item.
    if ($node->bundle() == 'islandora_object') {
      $resource_type = $node->get('field_resource_type')->entity->getName();
      // If resource type is Text.
      if ($resource_type == 'Text') {
        $model = $node->get('field_model')->entity->getName();
        // If model type is Digital Document
        if ($model == 'Digital Document') {
          // If display hint is selected as none.
          $display_hint = $node->field_display_hints->getValue();
          if (empty($display_hint)) {
            return AccessResult::allowed();
          }
        }
      }
    }
    return AccessResult::forbidden();
  }

}
