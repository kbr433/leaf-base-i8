(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.leaf_base = {
    attach: function(context, settings) {
      if(Drupal.behaviors.leaf_base.loaded) { return; }
      Drupal.behaviors.leaf_base.loaded = true;
      var $colnav = $('.collapsible-navigation');

      $('span#search-icon').on('click', function() {
        $colnav.removeClass('main account');
        if($('.collapsible-navigation.open.search').length > 0) {
          $colnav.removeClass('open');
        } else {
          $colnav.addClass('open');
        }
        $colnav.toggleClass('search');
      });

      $('button.navbar-toggler').on('click', function() {
        $colnav.removeClass('search account');
        if($('.collapsible-navigation.open.main').length > 0) {
          $colnav.removeClass('open');
        } else {
          $colnav.addClass('open');
        }
        $colnav.toggleClass('main');
      });

      $('span#user-icon').on('click', function() {
        $colnav.removeClass('main search');
        if($('.collapsible-navigation.open.account').length > 0) {
          $colnav.removeClass('open');
        } else {
          $colnav.addClass('open');
        }
        $colnav.toggleClass('account');
      });

      // Only show the project gallery filters when all projects are visible on the page.
      $('.view-footer button').on('click', function(ev) {
        ev.preventDefault();
        $('.pg-footer').hide();
        $('.hidden-item').removeClass('hidden-item');
        $('body').addClass('show-pg-filters');
      });

      if($('.slider-wrapper').length > 0) {
        $('.slider-wrapper').slick({
          dots: false,
          mobileFirst: true,
          infinite: true,
          centerMode: true,
          slidesToShow: 1,
          slidesToScroll: 1
        });
      }
    }
  }
})(jQuery, Drupal, drupalSettings);
