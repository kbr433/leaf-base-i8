<?php

/**
 * @file
 * Contains Drupal\leaf_content_lock\Form\LeafContentlockGroupTimeoutForm.
 */

namespace Drupal\leaf_content_lock\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Class LeafContentlockGroupTimeoutForm.
 */
class LeafContentlockGroupTimeoutForm extends FormBase {

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a Merge object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   A Connection object.
   * @param string $table
   *   Name of the table to associate with this query.
   * @param array $options
   *   Array of database options.
   */

  protected $database;

  /**
   * Constructs a new LeafContentlockGroupTimeoutForm object.
   */
  public function __construct(MessengerInterface $messenger, Connection $database) {
    $this->messenger = $messenger;
    $this->database = $database;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('database')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'leaf_contentlock_group_timeout_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $current_path = \Drupal::service('path.current')->getPath();

    $array_params = explode("/", $current_path);

    $query = $this->database->select('leaf_content_lock_timeout', 'lf');
    $query->fields('lf', array('timeout_duration'));
    $query->condition('entity_id', $array_params[2], "=");
    $result = $query->execute()->fetchField();
    
    $default_timeout = $result ?? 30;


    $form['timeout_value'] = [
      '#type' => 'textfield',
      '#attributes' => array(
        'type' => 'number'
      ),
      '#title' => $this->t('Lock timeout'),
      '#description' => t('The maximum time in minutes that each lock may be kept. To disable breaking locks after a timeout.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $default_timeout,
    ];

    $form['entity_id'] = [
      '#type' => 'hidden',
      '#title' => $this->t('entity_id'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $array_params['2'],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
    $this->database->merge('leaf_content_lock_timeout')
      ->key('entity_id', $form_state->getValue('entity_id'))
      ->fields([
          'timeout_duration' => $form_state->getValue('timeout_value'),
          'timestamp' => time(),
      ])
      ->execute();
    
    $this->messenger->addStatus('Timeout values saved successfully.');

  }
}
