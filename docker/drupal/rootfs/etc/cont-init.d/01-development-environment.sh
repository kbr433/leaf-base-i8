#!/usr/bin/with-contenv bash
# shellcheck shell=bash
set -e

# This only runs in the development environments.
if [[ "$DRUPAL_ENVIRONMENT" == "dev" ]]; then
  if ! getent passwd ${UID}; then
    usermod -u ${UID} nginx
    chown -R nginx:nginx /var/www
  fi
fi
