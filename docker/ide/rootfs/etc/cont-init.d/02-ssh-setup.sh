#!/usr/bin/with-contenv bash
# shellcheck shell=bash
set -ex

# We allow password-less authentication.
passwd -d nginx
