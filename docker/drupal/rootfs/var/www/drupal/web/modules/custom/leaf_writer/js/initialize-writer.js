/**
 * @file
 * Initialize the leaf writer library.
 */

((Drupal, once) => {
  const initializeElement = function (element) {
    const config = JSON.parse(element.dataset.leafWriterConfig);
    const xmlInput = element.querySelector('.leaf-writer--xml-input');
    const editor = new Leafwriter.Leafwriter(element.querySelector('.leaf-writer--container'))
    editor.init({
      document: {
        url: config.xml_file_url,
        xml: xmlInput.value,
      },
      settings: {
        baseUrl: '/libraries/leafwriter/dist',
        credentials: {nssiToken: '123'},
        lookups: {
          authorities: [['geonames', { config: { username: 'cwrcgeonames' } }]],
        },
      },
      user: {
        name: config.user.name,
        uri: config.user.id,
      }
    });

    // Update the xml value stored in the xml input on form submit.
    const form = xmlInput.closest('form');
    if (form) {
      form.addEventListener('submit', (e) => {
        editor.getContent().then((xml) => {
          xmlInput.value = xml;
          editor.setIsEditorDirty(false);
        });
      });
    }
  };

  /**
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.leafWriterInitializer = {
    attach: function (context) {
      once('leaf-writer-initialized', '[data-leaf-writer-config]').forEach(initializeElement);
    }
  };
})(Drupal, once);
